import pyodbc, os, types, collections, datetime
import dataclasses as dc

root=os.path.dirname(os.path.realpath(__file__))
mdb_file = f'{root}\\data-in\\2020-04-26.cfc814.mdb'
mdb_password = 'tvdm'
out_members_file = f'{root}\\data-out\\Members.csv'
out_licenses_file = f'{root}\\data-out\\MemberLicense.csv'
out_members_notes_file = f'{root}\\data-out\\Members.Notes.txt'
out_original_file = f'{root}\\data-out\\Original-data-from.cfc.mdb.csv'

g = types.SimpleNamespace()
g.filter = collections.defaultdict(int)
g.migrated = 0
g.transform = collections.defaultdict(int)
g.written = collections.defaultdict(int)
g.write_csv_headers = [True, True]      # one for each output file
g.mdb_driver = '{Microsoft Access Driver (*.mdb)}'
g.duplicates = {}
g.excluded_cfc_ids = [
    1, 102257, 128703, 140241, 140389, 142141,      # confirmed
    # 129146, 138630, 139133, 139847, 142230, 146264, 140395, 140572,   # are people
]

table_members = 'Membership Information'
canadian_prov = ('AB','BC','MB','NB','NL','NS','NT','NU','ON','PE','QC','SK','YT',)
dt_20200102 = datetime.datetime(2020,1,2)


def main():
    with open(out_members_notes_file, 'w') as f:
        now = datetime.datetime.now()
        div = '=' * 72
        f.write(f'{div}\nCreated: {now}\n')
    find_duplicates()
    with open(out_members_file,'w', encoding='utf8') as members_fo:
        with open(out_licenses_file,'w', encoding='utf8') as licenses_fo:
            for row in get_rows():
                if is_migrated(row):
                    transform_row(row)
                    put_row(row, members_fo, licenses_fo)
    show_stats()

def find_duplicates():
    type_priority = {
        'L':95,'A':90,'O':90,'o':90,'G':90,'P':89,
            'H':85,'T':75,'Z':50,'D':40,
        'J':20,'F':20,'B':20,'N':18,'R':18,
    }
    em_all = {}
    for row in get_rows():
        if not row.Email:
            continue
        if not is_migrated(row):
            continue
        email_lc = row.Email.strip().lower()
        if email_lc not in em_all:
            em_all[email_lc] = []
        em_all[email_lc].append((
            row.NUMBER,
            type_priority[row.TYPE],
        ))
    dups = {em: list for em, list in em_all.items() if len(list) > 1}
    g.duplicates = {}
    for email, list in dups.items():
        winner, priority = max(list, key=lambda x: x[1])
        g.duplicates[email] = winner

def is_migrated(row, show=False):
    if row.NUMBER in g.excluded_cfc_ids:     # ,139705
        g.filter['Specific CFC ids'] += 1    
    elif row.TYPE=='C':
        g.filter['TYPE=C (contacts only)'] += 1
    elif row.TYPE=='E':
        g.filter['TYPE=E (orgs, libraries, some people)'] += 1
    elif row.TYPE=='I':
        g.filter['TYPE=I (institutions)'] += 1
    elif row.TYPE=='M':
        g.filter['TYPE=M (equipment sales)'] += 1
    elif row.TYPE=='S':
        g.filter['TYPE=S (magazine subscription only)'] += 1
    elif row.TYPE=='Y':
        g.filter['TYPE=Y (?)'] += 1
    # elif row.TYPE=='Z' and row.EXPIRY.year == 1940:           # keep since some played rated games
    #     g.filter['TYPE=Z and year=1940'] += 1
    else:
        return True
    return False


def show_stats():
    print('Members EXCLUDED from migration:')
    for k, v in g.filter.items():
        print(f'\t{k}:  {v}')
    print('Members INCLUDED in migration')
    print(f'\tTotal migarating: {g.migrated}')
    print('Member data TRANSFORMS:')
    for k in sorted(g.transform.keys()):
        print(f'\t{k}:  {g.transform[k]}')
    print('Accounts & Licenses WRITTEN:')
    for k in sorted(g.written.keys()):
        print(f'\t{k}:  {g.written[k]}')


def transform_row(row):
    rnotes = row.Notes or ''
    if row.TYPE=='B':
        row.TYPE = 'J'
        row.Notes = f'[TYPE=B] {rnotes}'
        g.transform['TYPE=B-->J (kids tournaments, didn\'t buy a membership)'] += 1
    elif row.TYPE=='D':
        row.TYPE = 'A'
        row.Notes = f'[DECEASED] {rnotes}'
        g.transform['TYPE=D-->A (deceased; added to "Notes")'] += 1
    elif row.TYPE=='F':
        row.TYPE = 'J'
        row.Notes = f'[TYPE=F] {rnotes}'
        g.transform['TYPE=F-->J (family+junior membership)'] += 1
    elif row.TYPE=='G':
        row.TYPE = 'A'
        row.Notes = f'[TYPE=G] {rnotes}'
        g.transform['TYPE=G-->A (family; only if another adult in family)'] += 1
    elif row.TYPE=='H':
        row.TYPE = 'A'
        row.Notes = f'[TYPE=H] {rnotes}'
        g.transform['TYPE=H-->A (honourary)'] += 1
    elif row.TYPE=='N':
        row.TYPE = 'J'
        row.Notes = f'[TYPE=N] {rnotes}'
        g.transform['TYPE=N-->J (NAYCC; foreign players didn\'t need to buy a membership)'] += 1
    elif row.TYPE=='O' or row.TYPE=='o':
        row.TYPE = 'A'
        row.Notes = f'[TYPE=O] {rnotes}'
        g.transform['TYPE=O-->A (adults)'] += 1
    elif row.TYPE=='P':
        row.TYPE = 'A'
        row.Notes = f'[TYPE=P] {rnotes}'
        row.EXPIRY = dt_20200102
        g.transform['TYPE=P-->A (Quebec 3yr agreement; some may be juniors; change later)'] += 1
    elif row.TYPE=='R':
        row.TYPE = 'J'
        row.Notes = f'[TYPE=R] {rnotes}'
        row.EXPIRY = dt_20200102
        g.transform['TYPE=R-->J (Quebec 3yr agreement; juniors)'] += 1
    elif row.TYPE=='T':
        row.Notes = f'[TYPE=T] {rnotes}'
        g.transform['TYPE=T unchanged (tournament-only memberships)'] += 1
    elif row.TYPE=='Z':
        if row.EXPIRY and row.EXPIRY.year >= 1960:
            g.transform['TYPE=Z and EXPIRY>=1960: a "T" license will be created'] += 1
        row.TYPE = 'T'      # 2020-04-26: Bob says (so can follow-up)
        row.Notes = f'[TYPE=Z] {rnotes}'
        g.transform['TYPE=Z-->T (haven\'t paid but needed an id to do ratings)'] += 1

    if not row.Email:
        row.Email = ''
    email_lc = row.Email.strip().lower()
    if email_lc in g.duplicates:
        if row.NUMBER != g.duplicates[email_lc]:
            row.Email = ''

    # In .mdb file, some dates looked like typos; so fix 'em.
    if row.BIRTHDATE:
        if row.BIRTHDATE.year < 1015:
            # print(f'... CFC={int(row.NUMBER)} had birthdate fixed; was {row.BIRTHDATE}')
            row.BIRTHDATE = row.BIRTHDATE.replace(year=1000+row.BIRTHDATE.year)
        elif row.BIRTHDATE.year < 1900:
            # print(f'... CFC={int(row.NUMBER)} has birthdate fixed; was {row.BIRTHDATE}')
            row.BIRTHDATE = row.BIRTHDATE.replace(year=1900+(row.BIRTHDATE.year % 100))
        elif row.BIRTHDATE.year > 2020:
            print(f'... CFC={int(row.NUMBER)} has invalid birthdate {row.BIRTHDATE}')

    # In .mdb file, some Notes fields exceed GM's limit of 255 chars.  Migrate manually.
    if row.Notes and len(row.Notes) > 255:
        with open(out_members_notes_file, 'a') as f:
            div = ' ----' * 4
            f.write(f'\n{div} Notes for {row.NUMBER:.0f}{div}\n{row.Notes}')
        row.Notes = '[migrate manually]'

    return row


def get_rows():
    dbconn = get_dbconn(mdb_file, mdb_password)
    dbcsr = dbconn.cursor()

    sql = f'select * from "{table_members}"'
    dbcsr.execute(sql)
    for row in dbcsr.fetchall():
        # if row.NUMBER == 139705.0:
        #     print(f'\tRow: {row!r}')
        #     #print(dir(row))
        #     cols = [col[0] for col in row.cursor_description]
        #     print('Cols:', ', '.join(cols))
        yield row
    dbconn.close()


def put_row(row, members_fo, licenses_fo):
    g.migrated += 1
    r = row
    # ---- ---- ---- ---- Members
    csv = {}
    csv['MemberId'] = fmt_val(str(int(r.NUMBER)))
    csv['First Name'] = fmt_val(r.FIRST)
    csv['Surname'] = fmt_val(r.LAST)
    csv['Gender'] = fmt_val(r.SEX, sex=True)
    csv['DOB'] = fmt_val(r.BIRTHDATE)
    csv['EmailAddress'] = fmt_val(r.Email)
    csv['LoginId'] = fmt_val(r.Email or str(int(r.NUMBER)))
    csv['Address1'] = fmt_val(r.ADDRESS)
    csv['Address2'] = ''
    csv['Town'] = fmt_val(r.CITY)
    csv['County'] = fmt_val(r.PROV)
    csv['Postcode'] = fmt_val(r.POSTCODE)
    c = 'Canada' if (r.PROV or '') in canadian_prov \
        else 'USA' if (r.PROV or '') == 'US' \
        else (r.PROV or 'Foreign')
    g.transform[f'COUNTRY={c}'] += 1
    csv['Country'] = fmt_val(c)
    csv['Phone'] = fmt_val(r.PHONE, phone=True)
    # ---- Custom Fields
    csv['FIDE'] = fmt_val(r.__getattribute__('FIDE NUMBER'))
    #csv['LastExpiry'] = fmt_val(r.__getattribute__('LAST EXPIRY'))
    csv['Notes'] = fmt_val(r.Notes.strip() if r.Notes else '')

    if g.write_csv_headers[0]:
        out = ','.join([x for x in csv.keys()]) + '\n'
        members_fo.write(out)
        g.write_csv_headers[0] = False
    out = ','.join([x for x in csv.values()]) + '\n'
    members_fo.write(out)
    g.written['Number of "Accounts"'] += 1
    # mdb fields:
    # - discarded: BATCH, SOURCE, GIFT, TD TYPE, MISC, LAST EXPIRY, 
    #       RATING, INDICATOR, SPECIAL, FIDE RATING,
    #       ACT_RATING, ACT_INDIC, ACT_SPECIL, 
    #       Account Receivable, Account Payable, CC Number, CC Expiry, Auto Renew, Last Update
    # - member: NUMBER, FIRST, LAST, SEX, BIRTHDATE, Email, ADDRESS, CITY, PROV, POSTCODE, PHONE, 
    # - member-custom: Notes, FIDE NUMBER, 
    # - license: TYPE, EXPIRY, 

    # ---- ---- ---- ---- Licenses
    # 2020-04-22: Bob says Z's should have a license
    # if r.TYPE == 'Z':
    #    g.written['TYPE=Z account added but not license (unpaid tournament player added for rating purposes) '] += 1
    if not r.EXPIRY:
        g.written['EXPIRY=(empty) no date so no license created'] += 1
    elif r.EXPIRY.year < 1960:
        g.written['EXPIRY < 1960; a fake date so no license created'] += 1
    else:
        csv = {}
        csv['MemberId'] = fmt_val(str(int(r.NUMBER)))
        csv['Name'] = fmt_val(f'{r.LAST}, {r.FIRST}')
        csv['Type'] = fmt_val(r.TYPE)
        csv['StartDate'] = '"1970-01-01"' if r.TYPE == 'L' \
            else '"1970-01-01"' if r.EXPIRY.year > 2095 \
            else fmt_val(r.EXPIRY, less_days=14) if r.TYPE == 'T' \
            else fmt_val(r.EXPIRY, less_days=365)
        csv['EndDate'] = '"2099-12-31"' if r.TYPE == 'L' \
            else fmt_val(r.EXPIRY)

        if g.write_csv_headers[1]:
            out = ','.join([x for x in csv.keys()]) + '\n'
            licenses_fo.write(out)
            g.write_csv_headers[1] = False
        out = ','.join([x for x in csv.values()]) + '\n'
        licenses_fo.write(out)
        g.written['Number of "Licenses"'] += 1


def fmt_val(val, sex=False, phone=False, country=False, less_days=None):
    if type(val) == int:
        return str(val)
    elif type(val) == datetime.datetime:
        if less_days:
            val = val - datetime.timedelta(days=less_days)
            if less_days==365 and val.day==2:
                val = val.replace(day=1)    # adjust for leap years
        val = f'{val:%Y-%m-%d}'
    elif phone:       # In mdb, phone is a float!
        val = str(int(val)) if val else ''
        if len(val)==11 and val[0]=='1':
            val = val[1:]
        if len(val)==10:
            val = f'+1-{val[0:3]}-{val[3:6]}-{val[6:]}'
    elif sex:
        val = str(val or '').upper()
        val = val if val in ('M','F') else ''
    elif val and country:
        pass
    if type(val) == str:
        val = val.strip()
    return '"%s"' % val.replace('"', '""') if val else ''


def get_dbconn(file, password=None):
    # Ref: https://github.com/mkleehammer/pyodbc/wiki/Tips-and-Tricks-by-Database-Platform
    pyodbc.pooling = False
    driver = '{Microsoft Access Driver (*.mdb)}'
    dbdsn = f'Driver={driver};Dbq={file};'
    #print('>>>> DNS:', dbdsn)
    if password:
        dbdsn += f'Pwd={password};'
    return pyodbc.connect(dbdsn)

main()