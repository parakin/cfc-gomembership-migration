import pyodbc, os, types, collections

g = types.SimpleNamespace()
g.mdb_file='data-in/2020-04-01.tvdm.cfc803.mdb'
g.mdb_password = 'tvdm'

g.root=os.path.dirname(os.path.realpath(__file__))
g.ctr = collections.defaultdict(int)
g.table_members = 'Membership Information'

def main():
    dbconn = get_dbconn(f'{g.root}\\{g.mdb_file}', g.mdb_password)

    check_drivers()
    # list_tables(dbconn)
    # describe_table(dbconn, g.table_members)
    # list_player(dbconn)
    # duplicate_emails(dbconn)
    # update_player(dbconn)
    # list_players(dbconn)

def check_drivers():
    #msa_drivers = [x for x in pyodbc.drivers() if x.startswith('Microsoft Access Driver')]
    msa_drivers = [x for x in pyodbc.drivers() if 'ACCESS' in x.upper()]
    print(f'Available MS-Access drivers:\n{msa_drivers}')

def list_tables(dbconn):
    crsr = dbconn.cursor()
    for table_info in crsr.tables(tableType='TABLE'):
        print(f'Table: {table_info.table_name}: {table_info}')

def describe_table(dbconn, tablename):
    print(f'---- Table: {tablename}')
    crsr = dbconn.cursor()
    crsr.execute(f'select * from "{tablename}"')
    for col in crsr.description:
        print(f'\tcol: {col}')
        #print(f"'{col[0]}',", end='')
    print('')

def list_player(dbconn, cfc_id=106488):
    crsr = dbconn.cursor()
    #crsr.execute(f'select number, name from Players where number = 106488')
    crsr.execute(f'select * from "{g.table_members}" where number = {cfc_id}')
    count = 0
    for row in crsr.fetchall():
        #print(f'CFC: {row.number} - {row.name}')
        print('---- Row:')
        print(row)
        print('-- Email -->', row.Email)       # Col names are case sensitive
        print(type(row))
        # for col in row.items():
        #     print(f'col: {col}')
        count += 1
        #if count > 10: return

def duplicate_emails(dbconn):
    crsr = dbconn.cursor()
    crsr.execute(f'select NUMBER, Email, TYPE from "{g.table_members}"')
    emails = {}
    for row in crsr.fetchall():
        if not row.Email:
            continue
        if '+' in row.Email:
            print(f'Contains a "+": {row.NUMBER}: {row.Email}')
        email = row.Email.lower()
        if email in emails:
            emails[email].append((row.NUMBER, row.TYPE,))
        else:
            emails[email] = [(row.NUMBER, row.TYPE)]
    dups = {em: list for em, list in emails.items() if len(list) > 1}
    print(f'Duplicate emails: {len(dups)}')
    for em, list in dups.items():
        print(f'{em}: {list}')


def update_player(dbconn):
    crsr = dbconn.cursor()
    crsr.execute('update Players set "FIDE NUMBER" = 777 where number = 106488')
    dbconn.commit()

def get_dbconn(file, password=None):
    # Ref: https://github.com/mkleehammer/pyodbc/wiki/Tips-and-Tricks-by-Database-Platform
    pyodbc.pooling = False
    driver = '{Microsoft Access Driver (*.mdb)}'
    dbdsn = f'Driver={driver};Dbq={file};'
    if password:
        dbdsn += f'Pwd={password};'
    return pyodbc.connect(dbdsn)

main()